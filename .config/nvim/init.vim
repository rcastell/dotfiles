call plug#begin()
Plug 'christoomey/vim-tmux-navigator'
Plug 'tpope/vim-sensible'
Plug 'bling/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'tpope/vim-fugitive'
Plug 'bronson/vim-trailing-whitespace'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'Valloric/YouCompleteMe'
Plug 'mattn/emmet-vim'
Plug 'altercation/vim-colors-solarized'
Plug 'fatih/vim-go'
Plug 'avakhov/vim-yaml'

call plug#end()

"vim-airline
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 0
" certain number of spaces are allowed after tabs, but not in between
let g:airline#extensions#whitespace#mixed_indent_algo = 1

"ctrlp
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlPMixed'
let g:ctrlp_working_path_mode = 'ra'

" nerdree
" close vim if nerdtree is the only window
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif
" toggle
map <C-n> :NERDTreeToggle<CR>
" initial width
:let g:NERDTreeWinSize=20

"colorscheme
set background=dark
colorscheme solarized
let g:solarized_termcolors=256

"personal stuff
set colorcolumn=80
set number
"This unsets the "last search pattern" register by hitting return
nnoremap <CR> :noh<CR><CR>
" genius
nnoremap ; :
" http://usevim.com/2012/10/19/vim101-set-hidden/
set hidden
" space as leader
map <space> <leader>
" close buffer
nnoremap <leader>q :bd<CR>

filetype indent on
filetype plugin indent on

"toggle paste mode
set pastetoggle=<F10>
