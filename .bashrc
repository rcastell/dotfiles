#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

PATH=~/usr/local/bin:$PATH
export GOPATH=/home/workspace/go

# Enable color and human friendly output.
alias ls='ls -h --color=auto'
alias dir='dir -h --color=auto'
alias vdir='vdir -h --color=auto'
alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

#handy aliases
alias ll='ls -l'
alias la='ls -a'
alias lla='ls -la'

#safety!
alias rm='rm -i'
alias cp='cp -i'
alias mv='mv -i'
alias ln='ln -i'

source ~/.bash_prompt
source ~/.python_helpers.sh

#editor
export EDITOR='vim'
export SYSTEMD_EDITOR='vim'

#dates in history
export HISTTIMEFORMAT="%d/%m/%y %T "
#bigger history
export HISTSIZE=10000
export HISTFILESIZE=10000

#eos config for cernbox
export EOS_MGM_URL=root://eoshome.cern.ch
export EOS_HOME="/eos/user/r/rcastell"
eosfusebind -g

#kubernetes get new config (need to specify the project before)
k8config(){
	if [ -d "~/.kube/$1" ]; then
		mkdir -p ~/.kube/$1
		eval $(openstack coe cluster config --dir ~/.kube/$1 --force $1)
	else 
		export KUBECONFIG=~/.kube/$1/config
	fi
}

printf '\e[?2004l'

#personal aliases
alias gping='ping www.google.com'
alias cprsync='rsync -a --stats --progress'

#functions
extract () {
    if [ -f $1 ] ; then
        case $1 in
            *.tar.bz2)        tar xjf $1        ;;
            *.tar.gz)         tar xzf $1        ;;
            *.bz2)            bunzip2 $1        ;;
            *.rar)            unrar x $1        ;;
            *.gz)             gunzip $1         ;;
            *.tar)            tar xf $1         ;;
            *.tbz2)           tar xjf $1        ;;
            *.tgz)            tar xzf $1        ;;
            *.zip)            unzip $1          ;;
            *.Z)              uncompress $1     ;;
            *.7z)             7zr e $1          ;;
            *)                echo "'$1' cannot be extracted via extract()" ;;
        esac
    else
        echo "'$1' is not a valid file"
    fi
}

portslay () {
    kill -9 `lsof -i tcp:$1 | tail -1 | awk '{ print $2;}'`
}

