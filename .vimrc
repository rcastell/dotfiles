nmap \r :!tmux send-keys -t 1:0.0 C-p C-j <CR><CR>
" for learning not to use arrows
noremap <Up> <NOP>
noremap <Down> <NOP>
noremap <Left> <NOP>
noremap <Right> <NOP>
" virtual lines when wrapped, count movements else
noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')
"split navigations
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"remap W to w to save
command! W  write

let python_highlight_all=1
syntax on
set nocompatible              " required
filetype off                  " required
set hlsearch


set relativenumber

call plug#begin()

Plug 'tmhedberg/SimpylFold'
Plug 'vim-scripts/indentpython.vim'
" needs configuration after installation
Plug 'Valloric/YouCompleteMe'
Plug 'scrooloose/nerdtree'
Plug 'jistr/vim-nerdtree-tabs'
Plug 'kien/ctrlp.vim'
Plug 'tpope/vim-fugitive'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'christoomey/vim-tmux-navigator'
" Plug 'scrooloose/syntastic'
Plug 'gregsexton/MatchTag'
Plug 'vim-scripts/Emmet.vim'
Plug 'tpope/vim-obsession'
Plug 'altercation/vim-colors-solarized'
Plug 'Raimondi/delimitMate'
Plug 'tell-k/vim-autopep8'
Plug 'dhruvasagar/vim-table-mode'
Plug 'w0rp/ale'
" All of your Plugins must be added before the following line
call plug#end()            " required
filetype plugin indent on    " required

" opens new splits in the right place
set splitbelow
set splitright

" smart indenting works for python
set smartindent

" Enable folding
set foldmethod=indent
set foldlevel=99
nnoremap <space> za

" python specific settings
au BufNewFile,BufRead *.py
    \ set tabstop=4|
    \ set softtabstop=4|
    \ set shiftwidth=4|
    \ set textwidth=99|
    \ set expandtab|
    \ set autoindent|
    \ set fileformat=unix

" html and js specific settings (indenting on save)
autocmd FileType html setlocal shiftwidth=2 tabstop=2
au BufWritePre *.html :normal migg=G`i
au BufWritePre *.js :normal migg=G`i

set encoding=utf-8

let g:ycm_autoclose_preview_window_after_completion=1
map <leader>g  :YcmCompleter GoToDefinitionElseDeclaration<CR>

"let g:autopep8_disable_show_diff=1
let g:autopep8_aggressive = 3
autocmd FileType python noremap <buffer> <F8> :call Autopep8()<CR>

"ignore files in NERDTree
let NERDTreeIgnore = ['\.pyc$', '\~$']

set laststatus=2
set t_Co=256
let g:airline_powerline_fonts = 1
set ttimeoutlen=50
set background=dark

" ale settings
let g:ale_lint_on_save =1 
let g:ale_linters = {
\   'html': [],
\   'javascript': ['eslint'],
\   'python': ['flake8', 'mypy'],
\}

"markdown tables
function! s:isAtStartOfLine(mapping)
  let text_before_cursor = getline('.')[0 : col('.')-1]
  let mapping_pattern = '\V' . escape(a:mapping, '\')
  let comment_pattern = '\V' . escape(substitute(&l:commentstring, '%s.*$', '', ''), '\')
  return (text_before_cursor =~? '^' . ('\v(' . comment_pattern . '\v)?') . '\s*\v' . mapping_pattern . '\v$')
endfunction

inoreabbrev <expr> <bar><bar>
          \ <SID>isAtStartOfLine('\|\|') ?
          \ '<c-o>:TableModeEnable<cr><bar><space><bar><left><left>' : '<bar><bar>'
inoreabbrev <expr> __
          \ <SID>isAtStartOfLine('__') ?
          \ '<c-o>:silent! TableModeDisable<cr>' : '__'
